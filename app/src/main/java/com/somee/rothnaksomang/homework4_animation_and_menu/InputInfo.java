package com.somee.rothnaksomang.homework4_animation_and_menu;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;

public class InputInfo extends AppCompatActivity {
//  Declare variable
    int GALLERY=1;
    Button btnPickImage,btnRegister;
    EditText etName,etPhoneNumber,etClassName;
    CircleImageView civProflie;
    Uri imageUri=null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_info);
        setReference();
        getImage();
        sendObject();

    }
    public void getImage(){
        btnPickImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getImageFromGallery();
            }
        });

    }


    public void sendObject(){
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getvalidation()==true){
                    StudentInfo studentInfo=new StudentInfo(etName.getText().toString(),etPhoneNumber.getText().toString(),etClassName.getText().toString(),imageUri.toString());
                    Intent i=new Intent(getApplicationContext(),MainActivity.class);
                    i.putExtra("studentInfo",studentInfo);
                    startActivity(i);
                }
            }
        });
    }

//    Method Validate View in xml File
    public Boolean getvalidation(){
        Boolean check=false;
        if (etName.getText().toString().equals("")){
            etName.requestFocus();
            Toast.makeText(getApplicationContext(),"Name is request",Toast.LENGTH_SHORT).show();
            check= false;
        }else if(etPhoneNumber.getText().toString().equals("")){
            etPhoneNumber.requestFocus();
            Toast.makeText(getApplicationContext(),"Phone Number is request",Toast.LENGTH_SHORT).show();
            check= false;
        }else if(etClassName.getText().toString().equals("")){
            etClassName.requestFocus();
            Toast.makeText(getApplicationContext(),"Class Name is request",Toast.LENGTH_SHORT).show();
            check= false;
        }else if(imageUri==null){
            Toast.makeText(getApplicationContext(),"Image is request",Toast.LENGTH_SHORT).show();
            check= false;
        }else{
            check= true;
        }
        return check;
    }

//    Method Open Gallery use Action Pick
//    and must to add Permission EXTERNAL_CONTENT_URI at AndroidManifest.xml
    public void getImageFromGallery(){
        Intent i=new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i,GALLERY);
    }

//    Method get Image after Select Image from gallery
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==this.RESULT_CANCELED){
            return;
        }
        if(requestCode==1 && data!=null){
            imageUri=data.getData();
            try {
                Bitmap bitmap=MediaStore.Images.Media.getBitmap(this.getContentResolver(),imageUri);
                civProflie.setImageBitmap(bitmap);
                Toast.makeText(getApplicationContext(),"Pick Image Successful",Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                Toast.makeText(getApplicationContext(),"Pick Image is not Successful",Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }

    //reference variable in java to view in xml file
    public void setReference(){
        btnRegister=findViewById(R.id.btnRegister);
        btnPickImage=findViewById(R.id.btnPickImage);
        etName=findViewById(R.id.etName);
        etClassName=findViewById(R.id.etClassName);
        etPhoneNumber=findViewById(R.id.etPhoneNumber);
        civProflie=findViewById(R.id.civProfile);
    }

}
