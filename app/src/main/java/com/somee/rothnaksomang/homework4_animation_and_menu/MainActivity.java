package com.somee.rothnaksomang.homework4_animation_and_menu;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {

    LinearLayout layoutMain;
    TextView tvName,tvPhoneName,tvClassName;
    CircleImageView civProfile;
    StudentInfo studentInfo=null;
    ImageView ivProfile;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getValueFromInputInfo();
    }
    public void getValueFromInputInfo(){

//  Declare Variable
        layoutMain=findViewById(R.id.layoutMain);
        Intent i=null;
//  create linear Layout
        LinearLayout linearLayout=new LinearLayout(this);
        LayoutInflater inflater=getLayoutInflater();

        i=getIntent();
//  get value form another activity by id 'studentInfo'
        studentInfo=i.getParcelableExtra("studentInfo");
        if (studentInfo!=null){
            Toast.makeText(getApplicationContext(),"Successful",Toast.LENGTH_SHORT).show();
            View v=inflater.inflate(R.layout.card_linear,null);

//      reference variable to view in xml file
            tvName=v.findViewById(R.id.tvName);
            tvPhoneName=v.findViewById(R.id.tvPhoneNumber);
            tvClassName=v.findViewById(R.id.tvClassName);
            civProfile=v.findViewById(R.id.civProfile);

//      set value to view
            tvName.setText(studentInfo.getName());
            tvPhoneName.setText(studentInfo.getPhoneNUmber());
            tvClassName.setText(studentInfo.getClassName());
//          convert string to Uri
            Uri imageUri=Uri.parse(studentInfo.getProfile());
            civProfile.setImageURI(imageUri);
//      add a layout to another layout
            layoutMain.addView(v);
        }
    }

//  Method Create Option Menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.option_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

//  Method Handle Event Click From user
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//  Declare variable and reference
        ImageView ivProfile_Detail=findViewById(R.id.ivProfile);
        Animation animation;
        int id=item.getItemId();
        if(id==R.id.mnAddNew){
            Intent i=new Intent(getApplicationContext(),InputInfo.class);
            startActivity(i);
        }else if(id==R.id.mnRemove){
//      remove all child in view group
            removeAllChild();
        }else if(id==R.id.mnViewDetail){
            LinearLayout linearLayout=new LinearLayout(this);
            LayoutInflater inflater=getLayoutInflater();
            View v=inflater.inflate(R.layout.detail_linear,null);
//      reference variable to view in xml file
            tvName=v.findViewById(R.id.tvName);
            tvPhoneName=v.findViewById(R.id.tvPhoneNumber);
            tvClassName=v.findViewById(R.id.tvClassName);
            ivProfile=v.findViewById(R.id.ivProfile);

//      set value to view
            tvName.setText(studentInfo.getName());
            tvPhoneName.setText(studentInfo.getPhoneNUmber());
            tvClassName.setText(studentInfo.getClassName());
//          convert string to Uri
            Uri imageUri=Uri.parse(studentInfo.getProfile());
            ivProfile.setImageURI(imageUri);
//      remove all child in view group
            removeAllChild();
//      add a layout to another layout
            layoutMain.addView(v);
        }else if(id==R.id.submnFadeIn){
//      call animation in xml file and set to vie
            animation=AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_in);
            ivProfile_Detail.setVisibility(View.VISIBLE);
            ivProfile_Detail.startAnimation(animation);

        }else if(id==R.id.submnFadeOut){
//      call animation in xml file and set to view
            animation=AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_out);
            ivProfile_Detail.setVisibility(View.VISIBLE);
            ivProfile_Detail.startAnimation(animation);
        }else if(id==R.id.submnZoomIn){
//      call animation in xml file and set to view
            animation=AnimationUtils.loadAnimation(getApplicationContext(),R.anim.zoom_in);
            ivProfile_Detail.setVisibility(View.VISIBLE);
            ivProfile_Detail.startAnimation(animation);

        }else if(id==R.id.submnZoomOut){
//      call animation in xml file and set to view
            animation=AnimationUtils.loadAnimation(getApplicationContext(),R.anim.zoom_out);
            ivProfile_Detail.setVisibility(View.VISIBLE);
            ivProfile_Detail.startAnimation(animation);
        }
        return super.onOptionsItemSelected(item);
    }

//  Method remove all child in view group
    public void removeAllChild(){
//  Check in ViewGroup have child or not
//  value 0=null
        if(layoutMain.getChildCount()!=0){
//          remove all view in group View
            layoutMain.removeAllViews();
        }
    }
}
