package com.somee.rothnaksomang.homework4_animation_and_menu;

import android.os.Parcel;
import android.os.Parcelable;

public class StudentInfo implements Parcelable {
    private String name;
    private String phoneNUmber;
    private String className;
    private String profile;

    public StudentInfo(String name, String phoneNUmber, String className, String profile) {
        this.name = name;
        this.phoneNUmber = phoneNUmber;
        this.className = className;
        this.profile = profile;
    }

    protected StudentInfo(Parcel in) {
        name = in.readString();
        phoneNUmber = in.readString();
        className = in.readString();
        profile = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(phoneNUmber);
        dest.writeString(className);
        dest.writeString(profile);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<StudentInfo> CREATOR = new Creator<StudentInfo>() {
        @Override
        public StudentInfo createFromParcel(Parcel in) {
            return new StudentInfo(in);
        }

        @Override
        public StudentInfo[] newArray(int size) {
            return new StudentInfo[size];
        }
    };

    public String getName() {
        return name;
    }

    public String getPhoneNUmber() {
        return phoneNUmber;
    }

    public String getClassName() {
        return className;
    }

    public String getProfile() {
        return profile;
    }
}
